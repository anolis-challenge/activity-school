public static void bubbleSort(int [] c, int n) {
    for (int i=0 ; i<n-1 ;i++){
        for (int j=0 ; j<n-1-i ;j++){
            if (c[j]>c[j+1]) {
                int temp=c[j];
                c[j]=c[j+1];
                c[j+1]=temp;
            }
       }
    }
}
