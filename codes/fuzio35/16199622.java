public static void bubbleSort(int [] a, int n){
    int temp =0;
    for(int i = 0;i<n-1;i++){
        for(int j = n-1;j>i;j--){
            if(a[j]<a[j-1]){
                temp = a[j-1];
                a[j-1]=a[j];
                a[j]=temp;
            }
        }
    }
}
