/**  
 * 冒泡排序函数  
 * 通过多次遍历数组，比较并交换相邻元素，使得较大的元素逐渐“浮”到数组的末尾，达到排序的目的。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 外层循环，控制遍历次数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环，控制每次遍历时的比较和交换  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换a[j]和a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
