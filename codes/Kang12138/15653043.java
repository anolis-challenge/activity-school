/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
        // 外层循环，遍历所有的数
        for (int i = 0; i < n - 1; i++) {
        // 内层循环，进行当前轮的比较和交换
        for (int j = 0; j < n - 1 - i; j++) {
        // 如果当前数比下一个数大，交换它们
        if (a[j] > a[j + 1]) {
        // 交换 a[j] 和 a[j+1]
        int temp = a[j];
        a[j] = a[j + 1];
        a[j + 1] = temp;
        }
        }
        }
} //end


