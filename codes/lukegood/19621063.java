/**
 * 冒泡排序函数
 * 
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环，控制排序的轮数，每一轮将一个最大的数移到末尾
    for (int i = 0; i < n - 1; i++) {
        // 内层循环，比较相邻的两个元素，如果前一个元素大于后一个元素则交换
        for (int j = 0; j < n - 1 - i; j++) {
            if (a[j] > a[j + 1]) {
                // 交换两个元素
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} // end bubbleSort
