/**
  * 冒泡排序函数
  * aa bb cc
  * @param a 待排序的数组
  * @param n 待排序的数组长度
  */
public static void bubbleSort(int [] a, int n){
	if (n <= 1) {
            return; // 如果数组长度小于等于1，则不需要排序
        }

        for (int i = 0; i < n; ++i) {
            // 提前退出冒泡循环的标志位
            boolean flag = false;
            for (int j = 0; j < n - i - 1; ++j) {
                // 相邻元素两两对比，如果顺序不符合要求，就调整位置
                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    flag = true; // 表示有数据交换
                }
            }
            if (!flag) break; // 没有数据交换，提前退出
        }    
} //end
