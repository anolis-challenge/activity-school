/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    // 外层循环控制遍历的次数
    for (int i = 0; i < n - 1; i++) {
        boolean swapped = false; // 标记是否发生了交换
        
        // 内层循环进行相邻元素的比较与交换
        for (int j = 0; j < n - 1 - i; j++) {
            if (a[j] > a[j + 1]) { // 如果当前元素比下一个元素大，则交换它们
                // 交换元素
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                
                swapped = true; // 发生了交换
            }
        }
        
        // 如果在本轮排序中没有发生任何交换，说明数组已经有序，可以提前结束
        if (!swapped) break;
    }

} //end

