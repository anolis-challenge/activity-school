/**  
 * 冒泡排序函数  
 * 该函数通过冒泡排序算法对数组进行排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制比较轮数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制每轮的比较次数  
            // 如果前一个元素大于后一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换元素  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
