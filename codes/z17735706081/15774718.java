/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较，将较大的元素逐渐往后移动，从而实现排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 遍历整个数组  
    for (int i = 0; i < n - 1; i++) {  
        // 最后一轮比较后，最大的数已经在正确的位置，无需再比较  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 使用临时变量进行交换  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
