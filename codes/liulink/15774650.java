/**  
 * 冒泡排序函数  
 * 将一个整数数组按照从小到大的顺序进行排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果前一个元素比后一个元素大，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
