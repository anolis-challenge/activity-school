public static void bubbleSort(int[] a, int n) {
    boolean swapped;
    for (int i = 0; i < n - 1; i++) {
        swapped = false;
        for (int j = 0; j < n - 1 - i; j++) {
            // 比较相邻元素，如果顺序错误就交换
            if (a[j] > a[j + 1]) {
                // 交换 a[j] 和 a[j + 1]
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                swapped = true;
            }
        }
        // 如果在这一轮排序中没有发生交换，说明数组已经有序，可以提前结束
        if (!swapped) {
            break;
        }
    }
}
