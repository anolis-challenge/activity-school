/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
/*public static void bubbleSort(int [] a, int n){
	 if (array == null || n <= 1) {  
        	    return;  
        }  
	 for (int i = 0; i < n - 1; i++) {  
            for (int j = 0; j < n - i - 1; j++) {  
                if (array[j] > array[j + 1]) {  
                    int temp = array[j];  
                    array[j] = array[j + 1];  
                    array[j + 1] = temp;  
                }  
            }  
        }  
}*/ 
public static void bubbleSort(int[] array, int length) {  
        if (array == null || length <= 1) {  
            return;  
        }  
  
        for (int i = 0; i < length - 1; i++) {  
            for (int j = 0; j < length - i - 1; j++) {  
                if (array[j] > array[j + 1]) {  
                    // 交换 array[j] 和 array[j + 1]  
                    int temp = array[j];  
                    array[j] = array[j + 1];  
                    array[j + 1] = temp;  
                }  
            }  
        }  
    }  
