public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
        for(int i = 0; i < n; i++) {
                for(int j = 0; j < n - i - 1; ++j) {
                        if(a[j] > a[j + 1]) {
                                int tmp = a[j];
                                a[j] = a[j + 1];
                                a[j + 1] = tmp;
                        }
                }
        }
}
