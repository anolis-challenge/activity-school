/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 * @author Mobai
 * @version 1.0
 * @date 2024/12/26
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序

	// 检查输入的有效性
    if (n <= 1) return;

    // 外层循环控制遍历轮数
    for (int i = 0; i < n - 1; ++i) {
        // 内层循环控制每一轮的比较和交换
        boolean swapped = false;
        for (int j = 0; j < n - 1 - i; ++j) {
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置
                // 交换元素
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                swapped = true;
            }
        }
        // 如果在这一轮中没有发生任何交换，说明数组已经有序，可以提前结束排序
        if (!swapped) break;
    }

} //end
