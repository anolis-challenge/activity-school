/**  
 * 冒泡排序函数  
 * 将数组 a 按照升序排列  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 使用临时变量交换元素  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
