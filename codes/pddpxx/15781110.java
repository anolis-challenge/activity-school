/**  
 * 冒泡排序函数  
 * 对数组a进行冒泡排序，使其变为有序数组  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 遍历所有数组元素  
    for (int i = 0; i < n - 1; i++) {  
        // 假设数组已经有序，标志位  
        boolean sorted = true;  
        // 最后i个元素已经有序，无需比较  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换a[j]和a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发现了需要交换的元素，所以数组并非有序  
                sorted = false;  
            }  
        }  
        // 如果在一次遍历中没有发生交换，则数组已经有序，可以提前结束  
        if (sorted) {  
            break;  
        }  
    }  
} //end
