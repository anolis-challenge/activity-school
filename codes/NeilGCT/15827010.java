/**  
 * 冒泡排序函数  
 * 该函数使用冒泡排序算法对整数数组进行排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序的轮数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每一轮的比较次数  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
