/**  
 * 冒泡排序函数  
 * 冒泡排序是一种简单的排序算法，它重复地走访过要排序的元素，一次比较两个元素，如果他们的顺序错误就把他们交换过来。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标记是否有交换，用于优化，如果在一趟排序中没有发生交换，则数组已经有序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，标记为true  
                swapped = true;  
            }  
        }  
        // 如果在一趟排序中没有发生交换，则数组已经有序，可以提前结束  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
