/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环控制排序趟数
    for (int i = 0; i < n - 1; i++) {
        // 内层循环控制每一趟比较的次数
        for (int j = 0; j < n - i - 1; j++) {
            // 如果前面的元素大于后面的元素，则交换
            if (a[j] > a[j + 1]) {
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
}
