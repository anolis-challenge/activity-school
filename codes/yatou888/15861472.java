/**  
 * 冒泡排序函数  
 * 通过多次遍历数组，每次比较相邻元素并交换位置，使得较大的元素逐渐“浮”到数组的末尾。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 每一轮排序的标记，用于判断是否有交换发生  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记有交换发生  
                swapped = true;  
            }  
        }  
        // 如果这一轮没有发生交换，说明数组已经有序，直接退出循环  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
