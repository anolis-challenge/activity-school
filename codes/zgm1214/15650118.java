/**  
 * 冒泡排序函数  
 * 通过多次遍历数组，比较相邻元素，并交换它们（如果它们的顺序错误）来排序数组  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 外层循环控制遍历次数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环负责具体的相邻元素比较和交换  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换a[j]和a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
    // 当所有的循环都完成后，数组a已经被排序  
} // end       
