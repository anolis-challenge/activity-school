public class Color {
    public enum color{红,蓝,绿,黄,黑};
    public static void main(String arg[]){
        int i,j,k,num=0;
        color colors[]=color.values();
        System.out.println("从红，蓝，绿，黄，黑这五种颜色中取出三种不同颜色的排列有以下可能性：");
        for(i=0;i<colors.length;i++){
            for(j=0;j< colors.length;j++){
                if(j==i){
                    continue;
                }
                else{
                    for(k=0;k< colors.length;k++) {
                        if((k!=i) && (k!=j)){
                            num=num+1;
                            System.out.println( "第"+num + "种情况" + " " + colors[i] + " " + colors[j] + " " + colors[k]);
                        }
                    }
                }
            }
        }
    }
}

